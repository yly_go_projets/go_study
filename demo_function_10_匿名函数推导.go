package main

import "fmt"

func main() {
	func_demo()
	func_demo_02 := func_demo //函数本身也是一个变量
	func_demo_02()

	// 匿名函数
	func_demo_03 := func() {
		fmt.Println("我是匿名函数")
	}
	func_demo_03()

	//匿名函数自己调用自己,可以有参数的  加一个括号即可
	func(a, b int) {
		fmt.Println(a, b)
		fmt.Println("我是匿名函数02")
	}(1, 6)
    //匿名函数自己调用自己可以有返回需要传递给一个变量
	func_demo_04 := func(c, d int) int {
		return c + d
	}(6, 6)

	fmt.Println(func_demo_04)

}

func func_demo() {
	println("我是demo函数")
}
