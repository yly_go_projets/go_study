package main

import "fmt"

// 参考 https://www.runoob.com/go/go-operators.html

func main() {
	//算数运算符
	/*
	   +	相加	A + B 输出结果 30
	   -	相减	A - B 输出结果 -10
	   *	相乘	A * B 输出结果 200
	   /	相除	B / A 输出结果 2
	   %	求余	B % A 输出结果 0
	   ++	自增	A++ 输出结果 11
	   --	自减	A-- 输出结果 9
	*/

	//a := 10
	//b := 3
	//
	//println(a + b)
	//println(a - b)
	//println(a * b)
	//println(a / b)
	//println(a % b)
	//a++ // a=a+1
	//println(a)
	//a-- // a=a-1
	//println(b)

	//----------------------------------------------------------

	// 关系运算符

	/*
			==	检查两个值是否相等，如果相等返回 True 否则返回 False。	(A == B) 为 False
			!=	检查两个值是否不相等，如果不相等返回 True 否则返回 False。	(A != B) 为 True
			>	检查左边值是否大于右边值，如果是返回 True 否则返回 False。	(A > B) 为 False
			<	检查左边值是否小于右边值，如果是返回 True 否则返回 False。	(A < B) 为 True
			>=	检查左边值是否大于等于右边值，如果是返回 True 否则返回 False。	(A >= B) 为 False
			<=	检查左边值是否小于等于右边值，如果是返回 True 否则返回 False。	(A <= B) 为 True
		  关系运算符 结果都是bool
	*/

	//c := 10
	//d := 8
	//println(c == d)
	//println(c != d)
	//println(c < d)
	//println(c >= d)

	//----------------------------------------------------------
	/*
		逻辑运算符
		下表列出了所有Go语言的逻辑运算符。假定 A 值为 True，B 值为 False。

		运算符	描述	实例
		&&	逻辑 AND 运算符。 如果两边的操作数都是 True，则条件 True，否则为 False。	(A && B) 为 False
		||	逻辑 OR 运算符。 如果两边的操作数有一个 True，则条件 True，否则为 False。	(A || B) 为 True
		!	逻辑 NOT 运算符。 如果条件为 True，则逻辑 NOT 条件 False，否则为 True。	!(A && B) 为 True
	*/

	//var a bool = true
	//var b bool = false
	//
	//if a && b {
	//	fmt.Printf("第一行 - 条件为 true\n") //走不进来的
	//}
	//if a || b {
	//	fmt.Printf("第二行 - 条件为 true\n")
	//}
	///* 修改 a 和 b 的值 */
	//a = false
	//b = true
	//if a && b {
	//	fmt.Printf("第三行 - 条件为 true\n")
	//} else {
	//	fmt.Printf("第三行 - 条件为 false\n")
	//}
	//if !(a && b) {
	//	fmt.Printf("第四行 - 条件为 true\n")
	//}
	//if !a {
	//	println(!a) //取的是修改后的值
	//}

	//----------------------------------------------------------

	//位运算符 & | 与或非
	/*
			  二进制  0  1  逢二进一
			  位运算符: 二进制 上的 0 false  1 true
			  逻辑运算符: & 我和你 1 1 结果才为 1  ,0
			  | 我或者你  1 0 结果为1
			  60  0011 1100
			  13  0000 1101
			  ---------------
		      & 0000 1100 我和你 同时满足
		      | 0011 1101 我或你 我或你满足即可
	*/

	//var a uint = 60
	//var b uint = 13
	//var c uint = 0
	////位运算
	//c = a & b
	//println("-----------------")
	//fmt.Printf("%d,二进制%b", c, c) // 0000 1100
	//println()
	//println("-----------------")
	//c = a | b
	//fmt.Printf("%d,二进制%b", c, c) // 0011 1101

	//----------------------------------------------------------

	// 赋值运算符
	/*
		    运算符	描述	  实例
			=	简单的赋值运算符，将一个表达式的值赋给一个左值	C = A + B 将 A + B 表达式结果赋值给 C
			+=	相加后再赋值	C += A 等于 C = C + A
			-=	相减后再赋值	C -= A 等于 C = C - A
			*=	相乘后再赋值	C *= A 等于 C = C * A
			/=	相除后再赋值	C /= A 等于 C = C / A
			%=	求余后再赋值	C %= A 等于 C = C % A
			<<=	左移后赋值	C <<= 2 等于 C = C << 2
			>>=	右移后赋值	C >>= 2 等于 C = C >> 2
			&=	按位与后赋值	C &= 2 等于 C = C & 2
			^=	按位异或后赋值	C ^= 2 等于 C = C ^ 2
			|=	按位或后赋值	C |= 2 等于 C = C | 2
	*/

	var a int = 21
	var c int

	c = a
	fmt.Println(c) //21

	c += a
	fmt.Println(c) //42

	c -= a
	fmt.Println(c) //21
	c *= a
	fmt.Println(c) //441

}
