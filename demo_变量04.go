package main

func main() {
	//常量-程序运行时,不会被修改的变量
	/**
	  常量的数据类型可以使布尔型,数字型,(整数型,浮点型和复数) 和字符串类型
	  常量名称一般用大写字母标识
	*/

	//const URL string = "www.baidu.com" //显示定义-需要指定类型
	//const URL2 = "www.baidu.com"       //隐式定义
	//
	//const a, b, c = 3.14, "张三", false //同时定义多个常量
	//
	//println(URL, URL2)
	//
	//println(a, b, c)

	//##########################################################

	// 特殊常量 iota

	const (
		a = iota //0
		b = iota
		c = iota //只要有iota类型后面的值不管有没有被赋值都会自动赋值
		d        // 不定义的话会自动沿用上一个iota的值
		e
		f = "asdad" //iota=5
		g           //iota=6
		h = iota    //iota=7
		i           //iota=8
	)

	const (
		j = iota
		k
	)

	println(a, b, c, d, e, f, g, h, i)
	println("----------------------")
	println(j, k)

}
