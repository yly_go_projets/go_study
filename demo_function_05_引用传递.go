package main

import "fmt"

/*
  参数传递
  按照数据存储特点来分:
  - 值类型的数据:操作的是数据本身,int,string,bool,float64,array...
  - 引用类型的数据:操作的是数据的地址 slice,map,chan...
*/

func main() {
	// 引用传递  传入的是引用类型的数据,地址

	//切片,可以扩容的数组  切片是引用类型的数据
	s1 := []int{1, 2, 3, 4}
	fmt.Println("默认的数据", s1)
	update2(s1)
	fmt.Println("调用后的数据", s1)
}

func update2(s2 []int) {
	fmt.Println("传递的数据", s2)
	s2[0] = 100
	fmt.Println("修改后的数据", s2)
}
