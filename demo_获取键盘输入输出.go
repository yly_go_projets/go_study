package main

import "fmt"

func main() {

	/*
	  scanner 扫描仪,扫描接收键盘录入
	*/
	var x int
	var y float64
	fmt.Println("请输入两个数 1 整数，2 浮点数:")
	//变量取地址   &变量  指针,地址来修改和操作变量
	fmt.Scanln(&x, &y) //scanln 阻塞等待你的键盘输入  先输入一个值按下tab后再次输入一个浮点数
	fmt.Println("x:", x)
	fmt.Println("y:", y)
}
