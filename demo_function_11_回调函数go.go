package main

import "fmt"

func main() {
	r1 := add_func(1, 2)
	fmt.Println(r1)

	r2:=oper(1,4,add_func) //类似于工厂方法
	fmt.Println(r2)
	r3:=oper(5,2,sub_func) //类似于工厂方法
	fmt.Println(r3)
	r4:=oper(12,2, func(i int, i2 int) int {
		if i2 == 0 {
			fmt.Println("除数不能为0")
			return 0
		}
		return i / i2
	}) //类似于工厂方法
	fmt.Println(r4)

}

//高阶函数,可以接收一个函数作为参数

func oper(a, b int, fun func(int, int) int) int {
	r := fun(a, b)
	return r
}

func add_func(a, b int) int {
	return a + b
}
func sub_func(a, b int) int {
	return a - b
}
