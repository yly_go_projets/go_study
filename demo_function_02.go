package main

import "fmt"

func main() {
	fmt.Println(max(2, 1)) //函数中实际传递的参数叫做实参
}

/*
  取最大值函数
  函数内传递的值叫做形参
*/
func max(num1, num2 int) int {
	var result int
	if num1 > num2 {
		result = num1
	} else {
		result = num2
	}
	return result
}
