package main

import "fmt"

/*
  参数传递
  按照数据存储特点来分:
  - 值类型的数据:操作的是数据本身,int,string,bool,float64,array...
  - 引用类型的数据:操作的是数据的地址 slice,map,chan...
*/

func main() {
	// 值传递
	/*
	   arr2的数据是从arr1复制来的,所以是不同的空间
	   修改arr2并不会影响arr1
	   值传递:传递的是数据的副本,修改数据,对于原始数据没有影响
	 */
	// 定义一个数组  [个数] 类型
	arr := [4]int{1, 2, 3, 4}
	fmt.Println(arr) //[1 2 3 4]
	update(arr)
    fmt.Println("调用修改后的数据为:",arr) //调用修改后的数据为: [1 2 3 4]
}

func update(arr2 [4]int) {
	fmt.Println("arr2接收的数据为", arr2)
	arr2[0] = 100
	fmt.Println("arr2修改后的数据为", arr2)

}
