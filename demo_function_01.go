package main

import "fmt"

//函数  https://www.runoob.com/go/go-functions.html
/*
    func 函数名(参数,参数...) 函数调用后的返回值{
     函数体:执行一段代码
     return 返回结果
   }
*/

func main() {
	fmt.Println(add(1, 5))
	printinfo()
	myprint("你好我是xxx")
	fmt.Println(add2(5, 6))

	//x,y:=swap(",world", "hello")
	//fmt.Println(x,y)
	x, _ := swap(",world", "hello") //_匿名函数
	fmt.Println(x)
}

/*
  无参无返回值函数
*/
func printinfo() {
	fmt.Println("func printinfo")
}

/*
 有一个参数的函数
*/
func myprint(msg string) {
	fmt.Println(msg)
}

/**
  有两个参数的函数
*/

func add(a int, b int) int {
	return a + b
}

/*
 有一个返回值的函数
*/
func add2(a, b int) int {
	c := a + b
	return c
}

/*
 有多个返回值的函数
*/

func swap(x, y string) (string, string) {
	return y, x
}
