package main

import "fmt"

//我是单行注释,程序不会执行这行语句,是写给自己看得

/*
我是多行注释
第一行 没有你
第二行 没有你
第三行 没有也罢
*/
func main() {
	fmt.Print("hello world")
}
