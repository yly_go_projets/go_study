package main

import "fmt"

func main() {
	r1 := increment()
	fmt.Println(r1)
	v1 := r1()
	fmt.Println(v1) //1
	v2 := r1()
	fmt.Println(v2) //2
	println("----------------")
	r2 := increment()
	v3 := r2()
	fmt.Println(v3) //1
	v4 := r2()
	fmt.Println(v4) //2
	fmt.Println(r1())

}

//自增 返回闭包类型返回为int类型

func increment() func() int {
	//局部变量1
	i := 0
	//定义一个匿名函数,给变量自增并返回
	fun := func() int { //内层函数,没有执行的
		i++
		return i
	}
	return fun
}
