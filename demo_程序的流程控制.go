package main

import "fmt"

func main() {
	/*
	  程序的流程控制结构一共有三种:顺序结构,选择结构,循环结构
	  顺序结构:从上到下,逐行执行,默认的逻辑
	  选择结构:条件满足某些代码才会执行
	  - if
	  - switch
	  - select ,后面channel再讲
	*/

	var a int = 15

	if a > 20 {
		fmt.Println("a>20")
	}
	if a < 20 {
		fmt.Println("a<20")
	}

	var b int = 30
	if b > 30 {
		fmt.Println("b>30")
	} else {
		fmt.Println("b<=30")
	}

	// 多条件判断
	// 分数
	var score int = 55
	// a b c d
	if score >= 90 && score <= 100 {
		fmt.Println("A")
	} else if score >= 80 && score < 90 {
		fmt.Println("B")
	} else if score >= 70 && score < 80 {
		fmt.Println("C")
	} else if score >= 60 && score < 70 {
		fmt.Println("D")
	} else {
		fmt.Println("E")
	}

	// if多重嵌套
	/**
	   if 布尔表达式 1{
	     //  布尔表达式 满足条件1执行
	     if 布尔表达式 2{
	    //  布尔表达式 满足条件2执行
	      }
	}
	*/

	//var c int = 100
	//var d int = 200
	//
	//if c == 100 {
	//	fmt.Println("满足条件一")
	//	if d == 200 {
	//		fmt.Println("满足条件二")
	//	}
	//}
	////多重嵌套案例-验证密码,再次输入密码
	//
	//var e, f int
	//
	//var pwd int = 123456
	////用户的输入
	//fmt.Print("请输入密码：")
	//fmt.Scanln(&e)
	//
	////业务:验证密码是否正确
	//if e == pwd {
	//	fmt.Print("请再次输入密码：")
	//	fmt.Scanln(&f)
	//	if f == pwd {
	//		fmt.Println("登录成功了")
	//	} else {
	//		fmt.Println("登录失败!第二次密码错误")
	//	}
	//} else {
	//	fmt.Println("登录失败!密码错误")
	//}

	//-------------------------------------------------------

	/**
	  switch语句
	   switch 语句用于基于不同条件执行不同动作，每一个 case 分支都是唯一的，从上至下逐一测试，直到匹配为止。
	   switch 语句执行的过程从上至下，直到找到匹配项，匹配项后面也不需要再加 break。
	   switch 默认情况下 case 最后自带 break 语句，匹配成功后就不会执行其他 case，如果我们需要执行后面的 case，可以使用 fallthrough
	*/
	var name string = "张三"
	switch name {
	case "李四":
		fmt.Println("im 李四")
	case "张三":
		fmt.Println("im 张三")
	case "王五", "孙六": //可以同时匹配两个值
		fmt.Println("im 张三")
	default:
		fmt.Println("im default")
	}

	//switch  fallthrough 示例

	var score_two int = 90
	switch score_two {
	case 90:
		fmt.Println("A01")
		fallthrough //会穿透到A02
	case 80:
		fmt.Println("A02")
	case 70:
		fmt.Println("B")
	default:
		fmt.Println("C")
	}

	//-------------------------------------------------------
	//循环结构:条件满足某些代码会被反复执行0-N次
	/**
	  参考 https://www.runoob.com/go/go-loops.html
	  for
	*/

	for i := 1; i <= 10; i++ { //循环打印1-10
		fmt.Println(i)
	}

	println("----------------------")
	// 计算1-10之间的数字之和
	var he int = 0

	for i := 1; i <= 10; i++ {
		he += i
	}
	fmt.Println("一到10之间的数字之和为：", he)

	println("----------------------")

	/**
	  for 循环案例
	  打印一个方阵  解决方案 双重for循环  第一层循环(外层循环控制行数) 第二层循环(内层循环) 控制列数
	  * * * * *
	  * * * * *
	  * * * * *
	  * * * * *
	*/
	println("-------打印一个方阵---------------")
	for i := 1; i <= 4; i++ {
		for j := 1; j <= 5; j++ {
			fmt.Print("* ")
		}
		println()
	}
	println("----------------------")

	/**
	  for 循环案例
	  打印99乘法表  解决方案 双重for循环  第一层循环(外层循环控制行数) 第二层循环(内层循环) 控制列数
	----------------------
	1*1=1
	1*2=2   2*2=4
	1*3=3   2*3=6   3*3=9
	1*4=4   2*4=8   3*4=12  4*4=16
	1*5=5   2*5=10  3*5=15  4*5=20  5*5=25
	1*6=6   2*6=12  3*6=18  4*6=24  5*6=30  6*6=36
	1*7=7   2*7=14  3*7=21  4*7=28  5*7=35  6*7=42  7*7=49
	1*8=8   2*8=16  3*8=24  4*8=32  5*8=40  6*8=48  7*8=56  8*8=64
	1*9=9   2*9=18  3*9=27  4*9=36  5*9=45  6*9=54  7*9=63  8*9=72  9*9=81
	-----------------------

	*/

	for i := 1; i <= 9; i++ {
		for j := 1; j <= i; j++ {
			//fmt.Print(j,"*",i,"=",i*j," ")
			fmt.Printf("%d*%d=%d \t", j, i, i*j)
		}
		println()
	}
	println("-----------------------")

	// 遍历string类型
	/*
	  获取字符串的长度 len
	  获取字符串的指定字节 str[0] 打印出来是字节编码(ASCLL) 然后转化为字节 fmt.Printf("%c",str[i])
	*/
	str := "hello,world"
	for i := 0; i < len(str); i++ {
        fmt.Printf("%c",str[i])
        println()
	}

	// for range循环,遍历数组,切片... 类似于 php的foreach

	map1 := make(map[int]float32)
	map1[1] = 1.0
	map1[2] = 2.0
	map1[3] = 3.0
	map1[4] = 4.0

	// 读取 key 和 value
	for key, value := range map1 {
		fmt.Printf("key is: %d - value is: %f\n", key, value)
	}

	// 读取 key
	for key := range map1 {
		fmt.Printf("key is: %d\n", key)
	}

	// 读取 value
	for _, value := range map1 {
		fmt.Printf("value is: %f\n", value)
	}

}
