package main

import "fmt"

/*
  defer函数或者方法:一个函数或方法的执行被延迟了
  - 你可以在函数中添加多个defer语句,当函数执行到最后时,这些defer语句会按照逆序执行,最后该函数返回,特别是当你在进行一些打开资源的操作时,
遇到错误需要提前返回,在返回前你需要关闭相应的资源,不然很容易造成资源泄漏等问题
  - 如果有很多调用defer,那么defer是采用先进后出(栈)模式
*/
func main() {
	//f("1")
	//fmt.Println("2")
	//defer f("3") //会被延迟到最后执行
	//fmt.Println("4")
	//defer f("5") //会被延迟到最后执行
	//fmt.Println("6")
	//println("----------------------")
	//----------------------

	a:=10
	fmt.Println("a=",a)
	defer fTwo(a)
	a++
	fmt.Println("end a=",a)
}

func f(s string) {
	fmt.Println(s)
}
func fTwo(s int) {
	fmt.Println("函数里面的值:",s)
}
