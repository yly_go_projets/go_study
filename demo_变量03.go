package main

//全局变量

var global_name string = "全局变量"

func main() {
	//变量的作用域
	/**
	  作用域分为局部变量&全局变量
	*/

	//局部变量
	var name string = "yaoliuyang"
	var age = 27
	var global_name string = "局部的变量" //相当于把外部的全局变量给覆盖了,就近原则

	println(name, age)

	//use 全局变量

	println(global_name)

	aaa()

}

func aaa() {
	//println(name)        //无法调用局部变量
	println(global_name) //调用全局变量
}
