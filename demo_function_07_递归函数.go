package main

import "fmt"

/*
  递归函数
  定义:一个函数自己调用自己,就叫做递归函数
  注意:递归函数需要有一个出口,逐渐向出口靠近,没有出口就会形成死循环
*/
func main() {
	sum := getSum(5)
	fmt.Println(sum)
}

/*
  案例:求和递归
  求和: 1,2,3,4,5
  getSum(5)
  getSum(4)+5
  getSum(3)+4
  getSum(2)+3
  getSum(1)+1
*/

func getSum(n int) int {
	if n == 1 {
		return 1
	}
	fmt.Printf("%d-%d+%d \n", n, 1, n)
	return getSum(n-1) + n
}
