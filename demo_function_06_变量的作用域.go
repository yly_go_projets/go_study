package main

import "fmt"

/*
   局部变量:函数内部定义的变量,叫做局部变量
   全局变量:函数外部定义的变量,叫做全局变量
*/

//全局变量
var num int = 100

func main() {
	temp := 100 //函数体内的局部变量

	//if 语句,for循环语句定义的局部变量均为一次性变量只能语句内使用
	if b := 1; b <= 10 {
		//语句内的局部变量
		temp := 50
		fmt.Println(temp) //50  局部变量遵循就近原则
		fmt.Println(b)    //1
	}

	fmt.Println(temp) //100
    num:=20
	fmt.Println(num) //20
	f2() //30

}

func f1() {
	a := 1
	fmt.Println(a)
}

func f2() {
	//fmt.Println(a) //不能使用其他函数定义的变量
	num:=30
	fmt.Println(num)
}
