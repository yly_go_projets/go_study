package main

import (
	"github.com/go-vgo/robotgo" //https://github.com/go-vgo/robotgo
)

func main() {
	// 移动鼠标到指定位置
	robotgo.MoveMouse(100, 200)

	// 点击鼠标左键
	robotgo.MouseClick("left", true)

	// 双击鼠标右键
	robotgo.MouseClick("right", true, 2)

	// 按住鼠标左键
	robotgo.MouseToggle("down")

	// 释放鼠标左键
	robotgo.MouseToggle("up")

	// 滚动鼠标
	robotgo.ScrollMouse(10, "up")
}
