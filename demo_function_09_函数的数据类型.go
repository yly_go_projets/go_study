package main

import "fmt"

/*
   func() 本身就是一个数据类型
*/
func main() {
	//func_test01 如果不加括号,函数就是一个变量
	//func_test01()  如果加了括号那就成了函数的调用
	fmt.Printf("%T\n", func_test01) //func()
	fmt.Printf("%T\n", 10)          //int
	fmt.Printf("%T\n", "hello")     //string
	//定义函数类型的变量
	var f5 func(int, int)
	f5 = func_test01
	f5(1, 2)
}

func func_test01(a, b int) {
	fmt.Println(a, b)
}
