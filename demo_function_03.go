package main

import "fmt"

func main() {
	fmt.Println(mySum(1, 5, 2, 2))
}

/*
  可变参数:一个函数的参数类型确定,但个数不确定,就可以使用可变参数
  可变参数只能放到最后
  一个参数列表中最多只能有一个可变参数
*/
//例子 求和函数 无论传递多少个值就可以算出他的值
func mySum(nums ...int) int {
	sum := 0
	for i := 0; i < len(nums); i++ {
		fmt.Printf("第%d次的值是:%d \n",i,nums[i])
		sum += nums[i]
	}
	return sum
}
